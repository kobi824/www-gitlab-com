---
layout: handbook-page-toc
title: Anti-Abuse Group
description: "The Anti-Abuse group creates controls to prevent abuse of the GitLab product"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

Our goal is to provide Insider Threat features for your applications as well as GitLab itself. We will help proactively identify malicious activity, accidental risk, compromised user accounts or infrastructure components, anomalous use of the GitLab platform, and various high-risk behaviors where actionable remediation steps are possible.

## How we work
- We work in accordance with our [GitLab values](/handbook/values/).
- We work [transparently](/handbook/values/#transparency) with nearly everything public.
- We get a chance to work on the things we want to work on.
- We have a [bias for action](/handbook/values/#bias-for-action).
- We make data-driven decisions.
- Everyone can contribute to our work.


## Direction

[Group direction](https://about.gitlab.com/direction/anti-abuse/)

## Team members

The following people are permanent members of the Anti-Abuse Group:

| Who | Role |
| --- | --- |
| [Alex Buijs](https://about.gitlab.com/company/team/#alexbuijs) | Senior Fullstack Engineer |
| [Eugie Limpin](https://about.gitlab.com/company/team/#eugielimpin) | Senior Fullstack Engineer |
| [Hinam Mehra](https://about.gitlab.com/company/team/#hmehra) | Fullstack Engineer |
| [Jay Swain](https://about.gitlab.com/company/team/#jswain) | Engineering Manager |
| [Jensen Stava](https://about.gitlab.com/company/team/#jstava) | Sr. Product Manager |

## How to contact us

* Tag a team member in a merge request or issue
* Post a message in the #g_anti-abuse channel (internal only)

## Initial focus

Building the team and detecting and preventing spam and cryptomining abuse of the GitLab SaaS platform.

## Project management process

Our team uses a hybrid of Scrum for our project management process. This process follows GitLab's [monthly milestone release cycle](/handbook/marketing/blog/release-posts/#schedule).

<%= partial "handbook/engineering/metrics/partials/_cross_functional_dashboard.erb", locals: { filter_value: "anti-abuse" } %>

### Workflow

Our team use the following workflow stages defined in the [Product Development Flow](/handbook/product-development-flow/#workflow-summary):

### Issue boards

We use a team specific [issue board](https://gitlab.com/groups/gitlab-org/-/boards/4292845?not%5Bmilestone_title%5D=Backlog&label_name[]=group%3A%3Aanti-abuse&group_by=epic) to track issue progress on a daily basis. Issue boards are our single source of truth for the status of our work.

### Iteration

We follow the [iteration process](/handbook/engineering/development/principles/#iteration) outlined by the Engineering function.

### Refinement

Refinement is the responsibility of every team member. While planning out an epic or a feature we break it down into small consumable chunks. This process is challenging, and takes time. This is why we've set a [weekly refinement meeting](#team-meetings).

### Milestone Planning and Timeline

Our team follows the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline) as our group is dependent on the [GitLab self-managed release cycle](https://about.gitlab.com/upcoming-releases/).

We use [planning issues](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=Planning%20Issue) to discuss priorites (GitLab internal).

### Issue labels

We use issue labels to keep us organized. Every issue has a set of required labels that the issue must be tagged with. Every issue also has a set of optional labels that are used as needed.

**Required labels**
- Stage: `~devops::anti-abuse`
- Group: `~group::anti-abuse`

### Merge request labels

MR labels can mirror issue labels (which is automatically done when created from an issue), but only certain labels are required for correctly [measuring engineering performance](#measuring-engineering-performance).

**Required labels**
- Stage: `~devops::anti-abuse`
- Group: `~group::anti-abuse`

### Milestones

We tag each issue and MR with the planned milestone or the milestone at time of completion.

## Team Meetings

Our group holds synchronous meetings to gain additional clarity and alignment on our async discussions. We aspire to [record](https://about.gitlab.com/handbook/tools-and-tips/zoom/) all of our meetings as our team members are spread across several time zones and often cannot attend at the scheduled time.

We have a weekly team sync meeting with rotating [EMEA/AMER](https://drive.google.com/drive/folders/1nm7FRZ0f9T4ajbmJvz4LYLVWl5cXiXiQ?usp=sharing) and [AMER/APAC](https://drive.google.com/drive/folders/1wLdWWi3f6Aho6E2m4Xbhv1Nuoy_ZSC1e?usp=sharing) friendly time slots: Weds 14:30 UTC and Thurs 00:00 UTC.

We have a weekly refinement session Friday 00:00 UTC.

## Pipeline Validation Service responsibility

[PVS](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service) is an internal service that belongs to the Anti-abuse team. It’s a combination of heuristic-based (text matching, etc) and behavior-based rules (duplicate builds, etc). The [Trust and Safety team](https://about.gitlab.com/handbook/security/security-operations/trustandsafety/) leverages this service the most, and acts as the customer for feature requests.

### Heuristic rules

Due to the nature of cryptomining attacks, heuristics are going to change quickly and need to be implemented rapidly. Accordingly, T&S is invited to submit MR’s to PVS that are heuristic based, or alternatively [request these changes](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/new?issuable_template=pvs_miss) from the Anti-abuse team.

### Behavior rules

Behavior rules are more slow to change and potentially cast a much wider net (vs a very targeted heuristic rule). Changes to behavior rules are expected to come from T&S, and implemented by the Anti-abuse team.

### Severity and Priority

[Severity](https://about.gitlab.com/handbook/security/#severity-and-priority-labels-on-security-issues) and priority will be added on all issues/merge requests created by T&S so that Anti-abuse can act on them accordingly.

Priority will be based on impact and likelihood of the attacker returning.

### Iteration

Anti-abuse will periodically review the accuracy of PVS alerts to see where there are opportunities to reduce the False Positive rate, without impacting the true positives, and Trust and Safety will help provide the required information to do this.
